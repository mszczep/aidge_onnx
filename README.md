![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/pipeline.svg?ignore_skipped=true) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge ONNX library

You can find in this folder the python library that implements the ONNX import into Aidge. 

So far be sure to have the correct requirements to use this library
- Python 
- onnx
- aidge_core

## Pip installation

You will need to install first the aidge_core library before installing aidge_onnx.
Also, make sure that the install path was set before installing aidge_core library.
Then run in your python environnement : 
``` bash
pip install . -v
```