"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple

import aidge_core
import onnx
from aidge_onnx.node_import import auto_register_import

@auto_register_import("gemm")
def import_gemm(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}

    if 'transA' in attrs:
        if attrs['transA'].i != 0:
            print(f"Warning: Attribute 'transA' value {attrs['transA'].i} is not supported for operator gemm.")
            return None
        del attrs['transA']

    if 'transB' in attrs:
        if attrs['transB'].i != 1:
            print(f"Warning: Attribute 'transB' value {attrs['transB'].i} is not supported for operator gemm.")
            return None
        del attrs['transB']

    if 'alpha' in attrs:
        if attrs['alpha'].f != 1.0:
            print(f"Warning: Attribute 'alpha' value {attrs['alpha'].f} is not supported for operator gemm.")
            return None
        del attrs['alpha']

    if 'beta' in attrs:
        if attrs['beta'].f != 1.0:
            print(f"Warning: Attribute 'beta' value {attrs['beta'].f} is not supported for operator gemm.")
            return None
        del attrs['beta']

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator gemm.")
        return None

    nb_outputs = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[0]
    nb_inputs = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[1]

    no_bias = len(input_nodes) == 0

    fc_node = aidge_core.FC(nb_inputs, nb_outputs, nobias=no_bias, name=node_name)
    print(f"- {node_name} ({onnx_node.op_type})")
    return fc_node
