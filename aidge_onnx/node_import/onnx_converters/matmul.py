"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple
import numpy as np

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("matmul")
def import_matmul(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator matmul.")
        return None

    matmul_node = aidge_core.MatMul(name=node_name)
    print(f"- {node_name} ({onnx_node.op_type})")
    return matmul_node
