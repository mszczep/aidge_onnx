"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("batchnorm", "batchnormalization")
def import_batch_norm(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}

    if opset < 9:
        if 'spatial' in attrs:
            if attrs['spatial'].i == 1:
                del attrs['spatial']
            else:
                print(f"Warning: Attribute 'spatial' value {attrs['spatial'].i} is not supported for operator batchnorm.")
                return None
    if 'epsilon' in attrs:
        epsilon = attrs['epsilon'].f
        del attrs['epsilon']
    else:
        epsilon = 1e-05

    if 'momentum' in attrs:
        momentum = attrs['momentum'].f
        del attrs['momentum']
    else:
        momentum = 0.9

    if 'training_mode' in attrs:
        if attrs['training_mode'].i != 0:
            print(f"Warning: Attribute 'training_mode' value {attrs['training_mode'].i} is not supported for operator batchnorm.")
            return None
        del attrs['training_mode']

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator batchnorm.")
        return None

    # Do not use BatchNorm2D helper here, because it requires nb_features argument
    # nb_features = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[0]
    # nb_features can be obtained as shown above, but at the time of instanciation
    # of this operator, input_nodes[1] may still be None.
    # This may be the case if the input is not an ONNX initializer, but another
    # node (like Identity).
    batch_norm_node = aidge_core.Node(aidge_core.BatchNormOp2D(epsilon, momentum), name=node_name)
    print(f"- {node_name} ({onnx_node.op_type})")
    return batch_norm_node
