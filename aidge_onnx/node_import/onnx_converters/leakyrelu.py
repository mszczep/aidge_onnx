"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("leakyrelu")
def import_leaky_relu(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    alpha = 0.0

    for attr in onnx_node.attribute:
        if attr.name == "alpha":
            alpha = attr.f
        else:
            print(f"Warning: Attribute {attr.name} is not supported for operator leakyrelu.")
            return None

    my_node = aidge_core.LeakyReLU(negative_slope=alpha, name=node_name)
    print(f"- {node_name} ({onnx_node.op_type})")
    return my_node
