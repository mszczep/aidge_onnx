"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple

import aidge_core
import onnx
import numpy as np
from aidge_onnx.node_import import auto_register_import
from onnx import numpy_helper


@auto_register_import("constant")
def import_constant(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    print(f"- {node_name} ({onnx_node.op_type})")
    if(len(onnx_node.attribute) == 1):
        values = numpy_helper.to_array(onnx_node.attribute[0].t)
        if onnx_node.attribute[0].name == "value":
            print(f"val type: {values.dtype}")
            return aidge_core.Producer(aidge_core.Tensor(values) if values.shape != () else aidge_core.Tensor(np.array(values.item(), dtype=values.dtype)), node_name, True)
        elif onnx_node.attribute[0].name == "sparse_value":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_float":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_floats":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_int":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_ints":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_string":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        elif onnx_node.attribute[0].name == "value_strings":
            raise RuntimeError(f"The attribute {onnx_node.attribute[0].name} is not yet supported. Please create the conversion to Producer in node_converters/converters/contant.py or open an issue at: https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/-/issues")
        else:
            raise ValueError(f"The attribute name \"{onnx_node.attribute[0].name}\" does not exist, the ONNX may be ill formed, the accepted attribute names are listed at: https://github.com/onnx/onnx/blob/main/docs/Operators.md#attributes-16")
    else:
        raise ValueError("The number of attributes doesn't respect the doc https://github.com/onnx/onnx/blob/main/docs/Operators.md#Constant")


