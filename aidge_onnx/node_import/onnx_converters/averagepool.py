"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple
import numpy as np

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("averagepool")
def import_avg_pooling(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}
    if 'kernel_shape' in attrs:
        kernel_dims = attrs['kernel_shape'].ints
        del attrs['kernel_shape']
    else:
        print('Warning: Avg pool must have kernel_shape attr')
        return None 

    if 'strides' in attrs:
        stride_dims = attrs['strides'].ints
        del attrs['strides']
    else:
        # If not present, the stride defaults is 1 along each spatial axis.
        stride_dims = [1] * len(kernel_dims)

    if 'dilations' in attrs:
        dilation_dims = attrs['dilations'].ints
        del attrs['dilations']
    else:
        # If not present, the stride defaults is 1 along each spatial axis.
        dilation_dims = [1] * len(kernel_dims)

    if np.count_nonzero(dilation_dims - np.array(1)) > 0:
        print(f"Warning: Attribute 'dilations' value is not supported for operator averagepool.")
        return None

    padding_dims = [0] * 2*len(kernel_dims)
    if 'pads' in attrs:
        # `pads` format should be as follow [x1_begin, x2_begin...x1_end, x2_end,...]
        for i in range(0, len(kernel_dims)):
            padding_dims[2*i] = attrs['pads'].ints[i]
            padding_dims[2*i+1] = attrs['pads'].ints[len(kernel_dims)-1+i]
        del attrs['pads']

    if 'auto_pad' in attrs and attrs['auto_pad'].s in (b'NOTSET', b'SAME_UPPER', b'SAME_LOWER', b'VALID'):
        if attrs['auto_pad'].s != b'NOTSET' and np.count_nonzero(padding_dims) > 0:
            raise RuntimeError(f"Error: malformed ONNX: cannot have both non-zero 'pads' and 'auto_pad' different from 'NOTSET'.")

        for i in range(0, len(kernel_dims)):
            padding = kernel_dims[i] - stride_dims[i]
            floorHalfPadding = padding // 2

            if attrs['auto_pad'].s == b'SAME_UPPER':
                padding_dims[2*i] = floorHalfPadding
                padding_dims[2*i+1] = padding - floorHalfPadding
            elif attrs['auto_pad'].s == b'SAME_LOWER':
                padding_dims[2*i] = padding - floorHalfPadding
                padding_dims[2*i+1] = floorHalfPadding
        del attrs['auto_pad']

    if 'ceil_mode' in attrs:
        if attrs['ceil_mode'].i != 0:
            print(f"Warning: Attribute 'ceil_mode' value {attrs['ceil_mode'].i} is not supported for operator averagepool.")
            return None
        del attrs['ceil_mode']

    if 'count_include_pad' in attrs:
        if attrs['count_include_pad'].i != 0:
            print(f"Warning: Attribute 'count_include_pad' value {attrs['count_include_pad'].i} is not supported for operator averagepool.")
            return None
        del attrs['count_include_pad']

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator averagepool.")
        return None

    if np.count_nonzero(padding_dims) > 0:
        if f"PaddedAvgPooling{len(kernel_dims)}D" in dir(aidge_core):
            avg_pooling_node = aidge_core.__getattribute__(f"PaddedAvgPooling{len(kernel_dims)}D")(
                kernel_dims,
                name=node_name,
                stride_dims=stride_dims,
                padding_dims=padding_dims)
        else:
            print(f"Warning: PaddedAvgPooling{len(kernel_dims)}D is not supported.")
            return None
    else:
        if f"AvgPooling{len(kernel_dims)}D" in dir(aidge_core):
            avg_pooling_node = aidge_core.__getattribute__(f"AvgPooling{len(kernel_dims)}D")(
                kernel_dims,
                name=node_name,
                stride_dims=stride_dims)
        else:
            print(f"Warning: AvgPooling{len(kernel_dims)}D is not supported.")
            return None

    print(f"- {node_name} ({onnx_node.op_type})")
    return avg_pooling_node
