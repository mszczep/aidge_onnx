"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple
import numpy as np

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("reducemean")
def import_reducemean(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}

    axes = []
    if 'axes' in attrs:
        axes = attrs['axes'].ints
        del attrs['axes']
    else:
        print(f"Warning: couldnt find attribute axes for operator reducemean.")
        return None

    # keepdims is optional
    keepdims = 1
    if 'keepdims' in attrs:
        keepdims = attrs['keepdims'].i
        del attrs['keepdims']

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator reducemean.")
        return None

    if f"ReduceMean{len(axes)}D" in dir(aidge_core):
        reducemean_node = aidge_core.__getattribute__(f"ReduceMean{len(axes)}D")(
            axes,
            keepdims,
            name=node_name)
    else:
        print(f"Warning: ReduceMean{len(axes)}D is not supported.")
        return None

    print(f"- {node_name} ({onnx_node.op_type})")
    return reducemean_node
