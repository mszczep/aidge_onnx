"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple
import numpy as np

import aidge_core
import onnx
from aidge_onnx.node_import import auto_register_import

@auto_register_import("conv")
def import_conv(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.output[0]
    attrs = {attr.name : attr for attr in onnx_node.attribute}

    no_bias = False
    if len(input_nodes) == 2:
        no_bias = True
    elif len(input_nodes) == 3:
        no_bias = False
    else:
        print(f"Conv with {len(input_nodes)} inputs is not supported.")
        return None

    if 'kernel_shape' in attrs:
        kernel_dims = attrs['kernel_shape'].ints
        del attrs['kernel_shape']
    else:
        # If not present, should be inferred from input W.
        kernel_dims = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[2:]

    if 'strides' in attrs:
        stride_dims = attrs['strides'].ints
        del attrs['strides']
    else:
        # If not present, the stride defaults is 1 along each spatial axis.
        stride_dims = [1] * len(kernel_dims)

    if 'dilations' in attrs:
        dilation_dims = attrs['dilations'].ints
        del attrs['dilations']
    else:
        # If not present, the stride defaults is 1 along each spatial axis.
        dilation_dims = [1] * len(kernel_dims)

    if 'group' in attrs:
        group = attrs['group'].i
        del attrs['group']
    else:
        # (default is 1)
        group = 1

    padding_dims = [0] * 2*len(kernel_dims)
    if 'pads' in attrs:
        # `pads` format should be as follow [x1_begin, x2_begin...x1_end, x2_end,...]
        for i in range(0, len(kernel_dims)):
            padding_dims[2*i] = attrs['pads'].ints[i]
            padding_dims[2*i+1] = attrs['pads'].ints[len(kernel_dims)+i]
        del attrs['pads']

    if 'auto_pad' in attrs and attrs['auto_pad'].s in (b'NOTSET', b'SAME_UPPER', b'SAME_LOWER', b'VALID'):
        if attrs['auto_pad'].s != b'NOTSET' and np.count_nonzero(padding_dims) > 0:
            raise RuntimeError(f"Error: malformed ONNX: cannot have both non-zero 'pads' and 'auto_pad' different from 'NOTSET'.")

        for i in range(0, len(kernel_dims)):
            padding = kernel_dims[i] - stride_dims[i]
            floorHalfPadding = padding // 2

            if attrs['auto_pad'].s == b'SAME_UPPER':
                padding_dims[2*i] = floorHalfPadding
                padding_dims[2*i+1] = padding - floorHalfPadding
            elif attrs['auto_pad'].s == b'SAME_LOWER':
                padding_dims[2*i] = padding - floorHalfPadding
                padding_dims[2*i+1] = floorHalfPadding
        del attrs['auto_pad']

    if len(attrs) > 0:
        print(f"Warning: unsupported attribute(s): {attrs.keys()} for operator conv.")
        return None

    out_channels = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[0]
    if group == 1:
        # Conv
        in_channels = input_nodes[1][0].get_operator().get_output(input_nodes[1][1]).dims()[1]
        if (np.count_nonzero(padding_dims) > 0):
            if f"PaddedConv{len(kernel_dims)}D" in dir(aidge_core):
                conv_node = aidge_core.__getattribute__(f"PaddedConv{len(kernel_dims)}D")(
                    in_channels,
                    out_channels,
                    kernel_dims,
                    name=node_name,
                    stride_dims=stride_dims,
                    padding_dims=padding_dims,
                    dilation_dims=dilation_dims,
                    no_bias=no_bias
                )
                aidge_operator = conv_node.get_operator()
                micro_graph = aidge_operator.get_micro_graph()
                conv_op, pad_op = None, None
                c_conv_node = None
                for node in micro_graph.get_nodes():
                    if node.type() == "Conv":
                        conv_op = node.get_operator()
                        c_conv_node = node
                    elif node.type() == "Pad":
                        pad_op = node.get_operator()
                    else:
                        raise RuntimeError(f"Unsupported node type: {node.type()} inside PaddedConv.")
            else:
                print(f"Warning: PaddedConv{len(kernel_dims)}D is not supported.")
                return None
        else:
            if f"Conv{len(kernel_dims)}D" in dir(aidge_core):
                conv_node = aidge_core.__getattribute__(f"Conv{len(kernel_dims)}D")(
                    in_channels,
                    out_channels,
                    kernel_dims,
                    name=node_name,
                    stride_dims=stride_dims,
                    dilation_dims=dilation_dims,
                    no_bias=no_bias
                )
            else:
                print(f"Warning: Conv{len(kernel_dims)}D is not supported.")
                return None
    else: # ConvDepthWise
        if group != out_channels:
                print(f"Warning: 'group' attribute value {group} is not supported for operator conv.")
                return None

        in_channels = out_channels

        if (np.count_nonzero(padding_dims) > 0):
            if f"PaddedConvDepthWise{len(kernel_dims)}D" in dir(aidge_core):
                conv_node = aidge_core.__getattribute__(f"PaddedConvDepthWise{len(kernel_dims)}D")(
                    out_channels,
                    kernel_dims,
                    name=node_name,
                    stride_dims=stride_dims,
                    padding_dims=padding_dims,
                    dilation_dims=dilation_dims,
                    no_bias=no_bias
                )
            else:
                print(f"Warning: PaddedConvDepthWise{len(kernel_dims)}D is not supported.")
                return None
        else:
            if f"ConvDepthWise{len(kernel_dims)}D" in dir(aidge_core):
                conv_node = aidge_core.__getattribute__(f"ConvDepthWise{len(kernel_dims)}D")(
                    out_channels,
                    kernel_dims,
                    name=node_name,
                    stride_dims=stride_dims,
                    dilation_dims=dilation_dims,
                    no_bias=no_bias
                )
            else:
                print(f"Warning: ConvDepthWise{len(kernel_dims)}D is not supported.")
                return None

    print(f"- {node_name} ({onnx_node.op_type})")
    return conv_node
