import unittest
import aidge_onnx

import numpy as np
import onnx
from onnx import helper
from onnx import numpy_helper
from onnx import TensorProto

class test_converter_register(unittest.TestCase):

    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_converter(self):
        filename = "onnx_test_conv.onnx"

        DILATIONS = [1, 1]
        GROUPS = 1
        KERNEL_SHAPE = [1, 1]
        PADS = [0, 0, 0, 0]
        STRIDES = [1, 1]

        onnx_inputs = [helper.make_tensor_value_info("data", TensorProto.FLOAT, [1, 3, 10, 10])]
        onnx_outputs = [helper.make_tensor_value_info("conv_out", TensorProto.FLOAT, [1, 3, 10, 10])]
        onnx_nodes = [helper.make_node(
            name="Conv0",
            op_type="Conv",
            inputs=["data", "weights0"],
            outputs=["conv_out"],
        )]
        onnx_nodes[-1].attribute.append(helper.make_attribute("dilations", DILATIONS))
        onnx_nodes[-1].attribute.append(helper.make_attribute("group", GROUPS))
        onnx_nodes[-1].attribute.append(helper.make_attribute("kernel_shape", KERNEL_SHAPE))
        onnx_nodes[-1].attribute.append(helper.make_attribute("pads", PADS))
        onnx_nodes[-1].attribute.append(helper.make_attribute("strides", STRIDES))

        weight_value = np.float32(np.arange(9).reshape(3,3,1,1))
        onnx_initializers = [numpy_helper.from_array(weight_value, "weights0")]
        # Create the graph (GraphProto)
        onnx_graph = onnx.helper.make_graph(
            nodes=onnx_nodes,
            initializer=onnx_initializers,
            name=filename,
            inputs=onnx_inputs,
            outputs=onnx_outputs,
        )
        # Create the model (ModelProto)
        onnx_model = onnx.helper.make_model(
            onnx_graph,
            producer_name="aidge_onnx",
            producer_version="0.2.0"
        )

        onnx.save(onnx_model, filename)

        model = aidge_onnx.load_onnx(filename)

        aidge_node = model.get_node("conv_out")

        self.assertEqual(aidge_node.type(), "Conv")
        self.assertEqual(aidge_node.get_operator().get_attr("KernelDims"), KERNEL_SHAPE)

        weights_node = aidge_node.input(1)[0]
        self.assertEqual(weights_node.type(), "Producer")
        self.assertEqual(weights_node.name(), "weights0")
        weights_tensor = weights_node.get_operator().get_output(0)
        for i,j in zip(weight_value.flatten(), weights_tensor):
            self.assertAlmostEqual(i, j)


if __name__ == '__main__':
    unittest.main()
