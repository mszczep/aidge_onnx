import unittest
import aidge_onnx
import os
import urllib
import zipfile

import aidge_core
import aidge_backend_cpu

import numpy as np
import onnx
from onnx import helper
from onnx import numpy_helper
from onnx import TensorProto
from test_import_export import download_onnx_model


class test_models(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_mobilenet_v1(self):
        urllib.request.urlretrieve(
            "https://gist.github.com/olivierbichler-cea/811b555bc99df673a4554861e7cc7370/archive/abe23e37708a498485e534e879745fc881f0bd4a.zip", "models.zip")
        with zipfile.ZipFile("models.zip", 'r') as zip:
            for zip_info in zip.infolist():
                if zip_info.is_dir():
                    continue
                zip_info.filename = os.path.basename(zip_info.filename)
                zip.extract(zip_info, "models")

        onnx_filename = "models/mobilenet-vww-save-simp.onnx"

        model = aidge_onnx.load_onnx(onnx_filename, verbose=True)
        self.assertTrue(aidge_onnx.check_onnx_validity(onnx_filename))

        aidge_core.remove_flatten(model)
        input_node = aidge_core.Producer([1, 3, 96, 96], "X")
        input_node.add_child(model)
        model.add(input_node)

        # Configuration of the model
        model.set_datatype(aidge_core.DataType.Float32)
        model.set_backend("cpu")

        model.forward_dims()
        model.save("mobilenet-vww-save-simp")

        scheduler = aidge_core.SequentialScheduler(model)
        scheduler.generate_scheduling()
        scheduler.forward(forward_dims=False)
        scheduler.save_scheduling_diagram("schedule")

        self.assertEqual(len(scheduler.get_static_scheduling(0)), 114)


if __name__ == '__main__':
    unittest.main()
