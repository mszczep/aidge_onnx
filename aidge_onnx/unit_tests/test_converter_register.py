import unittest
import aidge_onnx


class test_converter_register(unittest.TestCase):

    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_converter(self):
        self.assertTrue('conv' in aidge_onnx.node_import.supported_operators())
        self.assertTrue('relu' in aidge_onnx.node_import.supported_operators())
        self.assertTrue('globalaveragepool' in aidge_onnx.node_import.supported_operators())
        self.assertTrue('Conv' in aidge_onnx.node_export.supported_operators())
        self.assertTrue('ReLU' in aidge_onnx.node_export.supported_operators())
        self.assertTrue('GlobalAveragePooling' in aidge_onnx.node_export.supported_operators())

    def test_registering(self):
        aidge_onnx.node_import.register_import("my_op", lambda x : x)
        self.assertTrue('my_op' in aidge_onnx.node_import.supported_operators())
        aidge_onnx.node_export.register_export("my_op", lambda x : x)
        self.assertTrue('my_op' in aidge_onnx.node_export.supported_operators())

if __name__ == '__main__':
    unittest.main()
