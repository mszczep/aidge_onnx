import onnx
import numpy as np

_AIDGE_DOMAIN = "ai.onnx.converters.aidge"

_MAP_NP_ONNX_TYPE = {
        np.dtype(np.float32): onnx.TensorProto.FLOAT,
        np.dtype(np.float64): onnx.TensorProto.DOUBLE,
        np.dtype(np.int8): onnx.TensorProto.INT8,
        np.dtype(np.int16): onnx.TensorProto.INT16,
        np.dtype(np.int32): onnx.TensorProto.INT32,
        np.dtype(np.int64): onnx.TensorProto.INT64,
        np.dtype(np.uint8): onnx.TensorProto.UINT8,
        np.dtype(np.uint16): onnx.TensorProto.UINT16,
        np.dtype(np.uint32): onnx.TensorProto.UINT32,
        np.dtype(np.uint64): onnx.TensorProto.UINT64,
        np.dtype(np.bool_): onnx.TensorProto.BOOL,
    }
_MAP_ONNX_NP_TYPE = {v: k for k, v in _MAP_NP_ONNX_TYPE.items()}


def onnx_to_aidge_model_names(model: onnx.ModelProto):
    """
    Change the name of each node of the model from onnx convention to aidge's one
    args :
        model : to modify
    return :
        model : modified
    """
    for i in model.graph.initializer:
        i.name = onnx_to_aidge_name(i.name)

    for n in model.graph.node:
        if len(n.name) > 0 and n.name[0].isdigit():
            new_name = "layer_" + n.name
            n.name = new_name
        for index, i in enumerate(n.input):
            n.input[index] = onnx_to_aidge_name(i)
        for index, o in enumerate(n.output):
            n.output[index] = onnx_to_aidge_name(o)

    for i in model.graph.input:
        i.name = onnx_to_aidge_name(i.name)

    for o in model.graph.output:
        o.name = onnx_to_aidge_name(o.name)

    return model

def numpy_to_onnx_type(np_dtype):
    if np_dtype not in _MAP_NP_ONNX_TYPE:
        raise ValueError(f"Unsupported NumPy dtype: {np_dtype}")
    onnx_type = _MAP_NP_ONNX_TYPE[np_dtype]
    return onnx_type

def onnx_to_numpy_type(onnx_type):
    if onnx_type not in _MAP_ONNX_NP_TYPE:
        raise ValueError(f"Unsupported ONNX TensorProto type: {onnx_type}")
    np_dtype = _MAP_ONNX_NP_TYPE[onnx_type]
    return np_dtype

def onnx_to_aidge_name(name: str) -> str:
    """
    Translates onnx node naming convention to aidge naming convention
    """
    name = name.replace("/", "_").replace(".", "_")
    name = name if (len(name) == 0 or not name[0].isdigit()) else "data_" + name
    return name
